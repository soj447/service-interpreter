package org.epam.serviceinterpreter.controller;


import org.epam.serviceinterpreter.service.InterpreterAppService;
import org.epam.serviceinterpreter.exception.InterpreterCannotReadFileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class InterpreterAppController {

    private InterpreterAppService interpreterAppService;

    @Autowired
    public InterpreterAppController(InterpreterAppService interpreterAppService){
        this.interpreterAppService=interpreterAppService;
    }

    //TEST with cURL
    //curl -F "pFile=@/pathToFile" -F "keyFile=@/pathToFile" -X POST http://localhost:8080/loadFiles/lang
    @PostMapping("/loadFiles/{lang}")
    public String findInterpreterByLangAndExecute(
            @PathVariable String lang,
            @RequestParam("pFile") MultipartFile programTextFile,
            @RequestParam("keyFile") MultipartFile passedKeysFile
    ){
        String programText = "";
        String passedKeys = "";
        try {
            programText = new String(programTextFile.getBytes());
        }catch (IOException e){
            throw new InterpreterCannotReadFileException("Failed to load program text file.");
        }
        try {
            passedKeys = new String(passedKeysFile.getBytes());
        }catch(IOException e){
            throw new InterpreterCannotReadFileException("Failed to load keyboard file.");
        }
        return interpreterAppService.execute(lang, programText, passedKeys);
    }

    //TEST with cURL
    //curl -F "pFile=@/pathToFile" -X POST http://localhost:8080/loadFile/lang
    @PostMapping("/loadFile/{lang}")
    public String findInterpreterByLangAndExecute(
            @PathVariable String lang,
            @RequestParam("pFile") MultipartFile programTextFile
    ){
        String programText = "";

        try {
            programText = new String(programTextFile.getBytes());
        }catch (IOException e){
            throw new InterpreterCannotReadFileException("Failed to load program text file.");
        }
        return interpreterAppService.execute(lang, programText, "");
    }
}
